# Math Libraries

NERSC supports a wide range of math libraries. Many are available
as [modules](../../environment/index.md).

 * [FFTW](./fftw/index.md)
 * [GNU Science Library (GSL)](https://www.gnu.org/s/gsl/manual/gsl-ref.html)
 * LAPACK/ BLAS/ ScaLAPACK
    * [MKL](./mkl/index.md)
    * [LibSci](./libsci/index.md)
 * [PETSc](./petsc/index.md)
 * [Trillinos](https://trilinos.github.io)
