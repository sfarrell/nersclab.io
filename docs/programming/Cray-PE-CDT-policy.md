# Cray Programming Environment (CDT) Software Update Policy

## Policy

In order to provide a predictable, stable and consistent programming environment while still making necessary software updates, NERSC will be updating the Cray Programming Environment, namely the Cray Developer Toolkit (CDT), and the Intel compilers on a set cadence on Cori. CDT consists of compilers, MPI, scientific and I/O libraries, profiling and debugging tools, etc. See [monthly CDT release notes](https://pubs.cray.com/browse/xc/article/released-cray-xc-programming-environments) for full list of software in each CDT.

* New CDT software will be installed every 3 months. The CDT releases in December, March, June, and September will be installed. The actual versions may vary due to software verification and critical bug fixes. The new versions will not be made the defaults when installed.

* New software defaults will be set twice a year: once in January at the Allocation Year Rollover (the previous year's September release) and once in July (the March release). The actual versions may vary due to software verification and critical bug fixes.

* No more than four CDT versions will be made available on the system at any given time, with the consideration of keeping past-default versions longer than non-past-default versions.  (So do the Intel compilers.)

* Seven days advance notice will be given to the users for the above software changes. However, in rare cases, e.g., to fix urgent security bugs or critical bugs, individual or full CDT software defaults may be as needed without a prior notice to users. Announcements will be always made after the changes.

### Notes

* This policy may evolve over time to better serve users' software needs.

* A critical bug refers to the software defect that affect critical functionality or critical data, for which there is no workaround or there is a workaround but requires significant changes to the existing applications or workflows used by many users.

* If you need a CDT version that we have removed from the system, please open a ticket with [NERSC Consulting](https://help.nersc.gov).

